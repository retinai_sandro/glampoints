#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 14:42:15 2018

@author: truongp
"""
import numpy as np
from matplotlib import pyplot as plt
import tensorflow as tf
from skimage.feature import peak_local_max
import cv2
import argparse
import imageio
import os


def horizontal_combine_images(img1, img2):

    ratio=img1.shape[0]/img2.shape[0]
    imgs_comb=np.hstack((img1, cv2.resize(img2,None,fx=ratio, fy=ratio)))
    return imgs_comb


def draw_keypoints(img, kp):
    '''
        arguments: gray images
                kp1 is shape Nx2, N number of feature points, first point in horizontal direction
                '''
    image_copy=np.copy(img)
    nbr_points=kp.shape[0]
    for i in range(nbr_points):
        image=cv2.circle(image_copy, (np.int32(kp[i,0]),np.int32(kp[i,1])), 3, (0,0,0),thickness=-1)
    return image


def compute_homography(kp1, kp2, des1, des2, method, ratio_threshold):

    def unilateral_matching(des1, des2, method, ratio_threshold):
        if method == 'ORB' or method == 'FREAK' or method == 'BRISK':
            distance_metrics=cv2.NORM_HAMMING
        else:
            distance_metrics=cv2.NORM_L2
            
        bf = cv2.BFMatcher(distance_metrics)
        matches = bf.knnMatch(des1, des2, k=2)
        good = [m for m,n in matches if (m.distance < ratio_threshold*n.distance)]
        return good

    # for cases when no homography is found
    good = None
    homography = np.zeros((3, 3))
    ratio_inliers = 0.0

    if des1 is not None and des2 is not None:
        if des1.shape[0]>2 and des2.shape[0]>2:
            good = unilateral_matching(des1, des2, method, ratio_threshold)
            if len(good) >= 4:
                src_pts = np.float32([ kp1[m.queryIdx] for m in good ]).reshape(-1,1,2)
                # shape is number_good_matchesx1x2 (two coordinates of corresponding keypoint)
                dst_pts = np.float32([ kp2[m.trainIdx] for m in good ]).reshape(-1,1,2)       
                
                H, inliers = cv2.findHomography(src_pts, dst_pts,cv2.RANSAC)
                if H is None:
                    H = np.zeros((3,3))
                    ratio_inliers = 0.0
                else:
                    inliers = inliers.ravel().tolist()
                    ratio_inliers = np.sum(inliers)/len(inliers) if len(inliers)!=0 else 0.0

                homography = H

    return homography, ratio_inliers, good


def draw_matches(img1, img2, kp1, kp2, matches):
    '''
    arguments: gray images
                kp1 is shape Nx2, N number of feature points, first point in horizontal direction
                matches is Dmatch object of length the number of matches
                '''
    h,w=img1.shape[:2]
    img=horizontal_combine_images(img1, img2)

    src_pts = np.float32([ kp1[m.queryIdx] for m in matches]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx] for m in matches]).reshape(-1,1,2)
    
    #shape Mx1x2 M number of matches 
    dst_pts[:,:,0]=dst_pts[:, :,0]+w

    for i in range(src_pts.shape[0]):
        img=cv2.line(img,(src_pts[i,0,0],src_pts[i,0,1]),(dst_pts[i,0,0], dst_pts[i,0,1]),(0,0,255),1)

    return img


def conv_conv_pool(input_layer, filters, training, pool, batch_norm, name):
    np.random.seed(0)
    np.random.RandomState(0)

    if batch_norm is True:
        conv1 = tf.layers.conv2d(inputs=input_layer, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib \
                                 .layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_1'.format(name))
        batch_conv1 = tf.layers.batch_normalization(conv1, training=training, name='batch_{}_1'.format(name))
        relu1 = tf.nn.relu(batch_conv1, name="relu_{}_1".format(name))
        conv2 = tf.layers.conv2d(inputs=relu1, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib. \
                                 layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_2'.format(name))
        batch_conv2 = tf.layers.batch_normalization(conv2, training=training, name='batch_{}_2'.format(name))
        relu2 = tf.nn.relu(batch_conv2, name="relu_{}_2".format(name))
    else:

        conv1 = tf.layers.conv2d(inputs=input_layer, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib \
                                 .layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_1'.format(name))
        relu1 = tf.nn.relu(conv1, name="relu_{}_1".format(name))
        conv2 = tf.layers.conv2d(inputs=relu1, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib. \
                                 layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_2'.format(name))
        relu2 = tf.nn.relu(conv2, name="relu_{}_2".format(name))

    if pool is False:
        return relu2
    else:
        pool = tf.layers.max_pooling2d(inputs=relu2, pool_size=[2, 2], strides=2, \
                                       padding='SAME', name='pool_{}'.format(name))
        return relu2, pool


def upconv_concat(input_A, input_B, n_filter, training, batch_norm, name):
    """Upsample `inputA` and concat with `input_B`
    Args:
        input_A (4-D Tensor): (N, H, W, C)
        input_B (4-D Tensor): (N, 2*H, 2*H, C2)
        name (str): name of the concat operation
    Returns:
        output (4-D Tensor): (N, 2*H, 2*W, n_filter + C2)
    """

    upscale = tf.layers.conv2d_transpose(inputs=input_A, filters=n_filter, kernel_size=[2, 2], \
                                         padding='same', strides=[2, 2], kernel_regularizer= \
                                             tf.contrib.layers.l2_regularizer(0.1), data_format='channels_last', \
                                         activation=None, trainable=True, use_bias=True, \
                                         kernel_initializer=tf.glorot_normal_initializer(),
                                         name='decon_{}'.format(name))

    A_shape = tf.shape(upscale)
    B_shape = tf.shape(input_B)

    # offsets for the top left corner of the crop
    offsets = tf.cast([0, (A_shape[1] - B_shape[1]) // 2, (A_shape[2] - B_shape[2]) // 2, 0], tf.int32)
    size = tf.cast([-1, B_shape[1], B_shape[2], n_filter], tf.int32)
    A_crop = tf.slice(upscale, offsets, size)

    concat = tf.concat([A_crop, input_B], axis=3, name="concat_{}".format(name))
    return concat


def Unet_model_4(input_layer, training, norm):
    conv1, pool1 = conv_conv_pool(input_layer, 8, training, pool=True, batch_norm=norm, name='1')
    conv2, pool2 = conv_conv_pool(pool1, 16, training, pool=True, batch_norm=norm, name='2')
    conv3, pool3 = conv_conv_pool(pool2, 32, training, pool=True, batch_norm=norm, name='3')
    conv4, pool4 = conv_conv_pool(pool3, 64, training, pool=True, batch_norm=norm, name='4')
    conv5 = conv_conv_pool(pool4, 128, training, pool=False, batch_norm=norm, name='5')
    up6 = upconv_concat(conv5, conv4, 64, training, batch_norm=norm, name='6')
    conv6 = conv_conv_pool(up6, 64, training, pool=False, batch_norm=norm, name='6')
    up7 = upconv_concat(conv6, conv3, 32, training, batch_norm=norm, name='7')
    conv7 = conv_conv_pool(up7, 32, training, pool=False, batch_norm=norm, name='7')
    up8 = upconv_concat(conv7, conv2, 16, training, batch_norm=norm, name='8')
    conv8 = conv_conv_pool(up8, 16, training, pool=False, batch_norm=norm, name='8')
    up9 = upconv_concat(conv8, conv1, 8, training, batch_norm=norm, name='9')
    conv9 = conv_conv_pool(up9, 8, training, pool=False, batch_norm=norm, name='9')
    output = tf.layers.conv2d(conv9, filters=1, kernel_size=[1, 1], padding='same', \
                              activation=None, kernel_regularizer=tf.contrib \
                              .layers.l2_regularizer(0.1), trainable=True, \
                              kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                              name='final')
    output_sigmoid = tf.nn.sigmoid(output, name='final_sigmoid')
    return output_sigmoid


def sift(image, kp_before):
    SIFT = cv2.xfeatures2d.SIFT_create()
    kp, des = SIFT.compute(image, kp_before)
    if des is not None:
        eps = 1e-7
        des /= (des.sum(axis=1, keepdims=True) + eps)
        des = np.sqrt(des)
    return kp, des


def non_max_suppression(image, size_filter, proba):
    non_max = peak_local_max(image, min_distance=size_filter, threshold_abs=proba, \
                             exclude_border=True, indices=False)
    kp = np.where(non_max > 0)
    if len(kp[0]) != 0:
        for i in range(len(kp[0])):

            window = non_max[kp[0][i] - size_filter:kp[0][i] + (size_filter + 1), \
                     kp[1][i] - size_filter:kp[1][i] + (size_filter + 1)]
            if np.sum(window) > 1:
                window[:, :] = 0
    return non_max


class GLAMpoints:
    def __init__(self, **kwargs):
        self.path_weights = str(kwargs['path_GLAMpoints_weights'])
        self.nms = int(kwargs['NMS'])
        self.min_prob = float(kwargs['min_prob'])
        tf.reset_default_graph()
        self.graph = tf.Graph()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess_detector = tf.Session(config=config, graph=self.graph)
        with self.graph.as_default():
            self.inputs = tf.placeholder(tf.float32, [None, None, None, 1], name='inputs')
            self.training = tf.placeholder(tf.bool, name="mode")
            self.kpmap = Unet_model_4(self.inputs, self.training, norm=True)
            saver = tf.train.Saver()
        with self.sess_detector.as_default():
            saver.restore(self.sess_detector, self.path_weights)
        # the network is restored

    def find_and_describe_keypoints(self, image, **kwargs):
        if len(image.shape) != 2:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image_norm = image / np.max(image)
        image_to_feed = np.zeros((1, image_norm.shape[0], image_norm.shape[1], 1))
        image_to_feed[0, :image_norm.shape[0], :image_norm.shape[1], 0] = image_norm

        with self.sess_detector.as_default():
            kp_map = self.sess_detector.run(self.kpmap, {self.inputs: image_to_feed, self.training: False})
        kp_map_nonmax = non_max_suppression(kp_map[0, :, :, 0], self.nms, self.min_prob)

        keypoints_map = np.where(kp_map_nonmax > 0)
        kp_array = np.array([keypoints_map[1], keypoints_map[0]]).T
        kp_cv2 = [cv2.KeyPoint(kp_array[i, 0], kp_array[i, 1], 10) for i in range(len(kp_array))]
        kp, des = sift(np.uint8(image), kp_cv2)
        kp = np.array([m.pt for m in kp], dtype=np.int32)
        return kp, des


class SIFT_noorientation:
    def __init__(self, **kwargs):
        self.sift = cv2.xfeatures2d.SIFT_create(nfeatures=int(kwargs['nfeatures']),
                                                contrastThreshold=float(kwargs['contrastThreshold']),
                                                edgeThreshold= float(kwargs['edgeThreshold']),
                                                sigma=float(kwargs['sigma']))

    def find_and_describe_keypoints(self, image):
        eps = 1e-7
        kp = self.sift.detect(image, None)
        kp = np.int32([kp[i].pt for i in range(len(kp))])
        kp = [cv2.KeyPoint(kp[i, 0], kp[i, 1], 10) for i in range(len(kp))]
        kp, des = self.sift.compute(image, kp)
        if des is not None:
            des /= (des.sum(axis=1, keepdims=True) + eps)
            des = np.sqrt(des)
        kp = np.array([kp[i].pt for i in range(len(kp))])
        return kp, des


class SIFT:
    def __init__(self, **kwargs):
        self.sift = cv2.xfeatures2d.SIFT_create(nfeatures=int(kwargs['nfeatures']),
                                                contrastThreshold=float(kwargs['contrastThreshold']),
                                                edgeThreshold= float(kwargs['edgeThreshold']),
                                                sigma=float(kwargs['sigma']))

    def find_and_describe_keypoints(self, image):
        eps = 1e-7
        kp = self.sift.detect(image, None)
        kp, des = self.sift.compute(image, kp)
        if des is not None:
            des /= (des.sum(axis=1, keepdims=True) + eps)
            des = np.sqrt(des)
        kp = np.array([kp[i].pt for i in range(len(kp))])
        return kp, des


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Computing matches and registration using GLAMpoints')
    parser.add_argument('--path_image1', type=str, default='path_to_image1',
    help='Path to the first image.')
    parser.add_argument('--path_image2', type=str, default='path_to_image1',
    help='Path to the second image.')
    parser.add_argument('--write_dir', type=str, default='path_to_result_dir',
    help='Directory where to write output figure.')
    parser.add_argument('--path_GLAMpoints_weights', type=str, default='weights/model-34',
    help='Path to pretrained weights file of GLAMpoint model (default: weights/model-34).')
    parser.add_argument('--SIFT', type=bool, default=True,
    help='Compute matches and registration with SIFT detector as a comparison? (default:True)')
    parser.add_argument('--NMS', type=int, default=15,
    help='Value of the NMS window applied on the score map output of GLAMpoint (default:15)')
    parser.add_argument('--min_prob', type=float, default=0.0,
    help='Minimum probability of a keypoint for GLAMpoint (default:0)')
    parser.add_argument('--green_channel', type=bool, default=True,
    help='Use the green channel (default:True)')
    opt = parser.parse_args()

    if not os.path.isdir(opt.write_dir):
        os.makedirs(opt.write_dir)

    kwarg=vars(parser.parse_args(args=None, namespace=None))
    kwarg_SIFT={'nfeatures':600, 'contrastThreshold':0.0275, 'edgeThreshold':12.0, 'sigma':1.0}
    

    try:
        image1 = imageio.imread(opt.path_image1)
        image2 = imageio.imread(opt.path_image2)
    except ValueError:
        print('cannot read your images')

    if opt.green_channel:
        image1_gray=image1[:,:,1]
        image2_gray=image2[:,:,1]
    else:
        image1_gray=cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
        image2_gray=cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)

    glampoints = GLAMpoints(**kwarg)

    # gets kp and descriptor from both images using glampoint
    kp1, des1 = glampoints.find_and_describe_keypoints(image1_gray)
    kp2, des2 = glampoints.find_and_describe_keypoints(image2_gray)
    im1 = np.uint8(draw_keypoints(cv2.cvtColor(image1, cv2.COLOR_RGB2BGR), kp1))
    im2 = np.uint8(draw_keypoints(cv2.cvtColor(image2, cv2.COLOR_RGB2BGR), kp2))

    # compute matches and homography
    homography, inlier_ratio, matches = compute_homography(kp1, kp2, des1, des2, method='GLAMpoint', ratio_threshold=0.8)

    fig, ((axis1, axis2, axis3), (axis4, axis5, axis6))=plt.subplots(2,3, figsize=(30,30))
    axis1.imshow(im1)
    axis1.set_title('detected kp on image 1, NMS={}'.format(opt.NMS))
    axis2.imshow(im2)
    axis2.set_title('detected kp on image 2, NMS={}'.format(opt.NMS))
    if matches is not None:
        img_matches = draw_matches(im1, im2, kp1, kp2, matches)
        axis3.imshow(img_matches)
        axis3.set_title('matches found')
    axis4.imshow(image1)
    axis4.set_title('image 1')
    axis5.imshow(image2)
    axis5.set_title('image 2')
    axis6.imshow(cv2.warpPerspective(image1, homography, (image1.shape[1], image1.shape[0])))
    axis6.set_title('image 1 transformed after \n estimating homography with RANSAC')
    fig.savefig(os.path.join(opt.write_dir, 'registration_GLAMpoints_NMS_{}_minprob_{}.png'.\
                format(opt.NMS, opt.min_prob)))
    plt.close(fig)

    if opt.SIFT:
        # compare to SIFT by extracting SIFT kp and descriptors
        sift = SIFT_noorientation(**kwarg_SIFT) # here for fair comparison, SIFT does not have orientation
        # can also be SIFT with orientation sift=SIFT(**kwarg_SIFT)
        kp1, des1=sift.find_and_describe_keypoints(image1_gray)
        kp2, des2=sift.find_and_describe_keypoints(image2_gray)
        im1=np.uint8(draw_keypoints(cv2.cvtColor(image1, cv2.COLOR_RGB2BGR), kp1))
        im2=np.uint8(draw_keypoints(cv2.cvtColor(image2, cv2.COLOR_RGB2BGR), kp2))

        homography, inlier_ratio, matches=compute_homography(kp1, kp2, des1, des2, 'RetiNet', 0.8)
        fig, ((axis1, axis2, axis3), (axis4, axis5, axis6))=plt.subplots(2,3, figsize=(30,30))
        axis1.imshow(im1)
        axis1.set_title('detected kp on image 1')
        axis2.imshow(im2)
        axis2.set_title('detected kp on image 2')
        if matches is not None:
            img_matches = draw_matches(im1, im2, kp1, kp2, matches)
            axis3.imshow(img_matches)
            axis3.set_title('matches found')
        axis4.imshow(image1)
        axis4.set_title('image 1')
        axis5.imshow(image2)
        axis5.set_title('image 2')
        axis6.imshow(cv2.warpPerspective(image1, homography, (image1.shape[1], image1.shape[0])))
        axis6.set_title('image 1 transformed after \n estimating homography with RANSAC')
        fig.savefig(os.path.join(opt.write_dir, 'registration_SIFT.png'))
        plt.close(fig)