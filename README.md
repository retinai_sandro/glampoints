# GLAMpoints: Greedily Learned Accurate Match points

This is the (unoptimized) reference implementation of the [ICCV 2019 paper "GLAMPoints: Greedily Learned Accurate Match points"](https://www.retinai.com/glampoints) 
([arXiv](https://arxiv.org/pdf/1908.06812.pdf), [supplemental](https://static1.squarespace.com/static/5967a5599de4bb65a7bb9736/t/5d7a6d2b9e03815777188115/1568304461382/Supplementary_Material.pdf)). 
It enables the training of a domain-specific keypoint detector over non-differentiable registration methods. 
This code exemplifies the presented method using root SIFT, a homography model and RANSAC optimization.


For a Pytorch implementation of GLAMpoints, visit [GLAMpoints_pytorch](https://github.com/PruneTruong/GLAMpoints_pytorch).

## Reminder of the steps of the training

![alt text](https://gitlab.com/retinai_sandro/glampoints/-/blob/master/images/summary_GLAMpoints.png)


== Preprocessing ==

- Load original data
- Crop and pad samples to 256x256


== Model ==
- Create 4-level deep Unet with batch normalization model f


== Data selection ==
At each iteration: 
- select original image 
- compute random geometric transformations g and g' (the maximum degree of the transformations can be chosen by the user depending on its test set)
- transform original image with g and g' to create respectively I and I'
- compute relation between images  I->I': g' * g^-1
- apply a subset of appearance changes: gaussian noise, changes of contrast, illumination, gamma, motion blur and the inverse of image.
Again, the maximum degree of the appearance changes can be chosen by the user. 


== Training ==

- compute S=f(I) and S'=f(I')
- compute NMS for both (N, N')
- compute root Sift descriptor N, N' (D, D')
- match points with (N, D), (N', D')
- find true positives and false positives, where true positives fullfill ||H*x - x'|| <= epsilon for epsilon=3
- compute reward map R
- compute mask M
- compute loss as L = (f(I)-R)**2)*M / sum(M, axis=[1,2,3])


## DOWNLOAD GIT LFS BEFORE CLONING THIS GIT

Before cloning this git, git lfs needs to be installed to download the model weights correctly. https://github.com/git-lfs/git-lfs/wiki/Installation

On Linux: 

```bash
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
git lfs install
```


## Training

To train the model on your dataset, prepare a folder with images (of approximately same height and width). 


Configure the file `config_training.yml` with at least `write_dir` and `path_images_training`. 
Please note that `path_images_training` must contain the path to the folder containing the reference images used to create the 
synthetic pairs of training images. **All** images contained in this folder will be used for the creation of training pairs. 
The same set-up must be performed for the test set. 

Adapt the hyperparameters in `training` to your dataset. 


**Note on the synthetic image pair creation:** At each epoch, random geometric transformations are applied to the original 
images so as to synthetically create the pairs of training images. One can adapt the degree of those geometric transformations 
by changing the parameters in `sample_homography` of the training_config. 
  Similarly, which appearance changes are applied to the training images as well as their strength can be adapted in`augmentation`. 
  **The degree of the geometric transformations should be chosen so that the resulting synthetic training set resembles the test set.**
In our case, our test set composed of retinal images only showed rotation up to 30 degrees and only little scaling changes, therefore we limited
the geometric transformations of the training set accordingly. 
However, **any degree of geometric transformation** or even non-linear ones can be applied to the original images to synthetically create pairs of training images. 



With this setup, run the script:

```bash
python training_glam_detector.py --path_ymlfile config_training.yaml --plot True
```

## Testing

### Detect GLAMpoints from a list of images

To test the pretrained model on a set of selected images, use the following script:

```bash
python compute_glam_kp.py --path_images /path/to/training/images/ --write_dir ///path/to/results/folder --NMS 10 --path_glam_weights /Users/dezanets/dev/glampoints/weights/model-34 --green_channel True
```

Outputs of this script are figures, showing for each image the detected keypoints. These figures are saved in --write_dir. It can also be a text file with the kp saved. 
The parameters are the following:
* --path_images can be path to a h5 file, a directory of images or a video (use codec). 
* --write_dir designates the directory where to save the images with the keypoints drawn on them
* --path_glam_weights is the path to the weights of the trained Reti model. 

optional arguments are
* --NMS which is the non max suppression window, default is 10
* --min_prob which is the minimum probability that a pixel must have to be considered as a keypoint on the score map (output of the Reti model), default is 0
* --green_channel: bool to determine if we are using the green channel instead of gray image to compute the score map (default is True).
* --save_text: bool to save a text file containing the matrix of kp extracted for each image. (default: False)


### Pipeline example of keypoint detection and matching from a pair of images

An example of the matching pipeline applied to a pair of images: compute_GLAMpoint_matches_and_registration.py

```bash
python compute_GLAMpoint_matches_and_registration.py --path_image1 /path/to/image1 --path_image2 /path/to/image2 --write_dir ///path/to/results/folder --NMS 10 --path_glam_weights /Users/dezanets/dev/glampoints/weights/model-34 --green_channel True
```

It extracts GLAMpoints out of both images as well as their corresponding root SIFT descriptor. Then the script looks for matches and estimates a homography transformation using RANSAC. 
It can also be compared to SIFT detector. The output is a figure showing the matches and registration. 
The parameters are the following:
* --path_image1 is a path to the first image of the pair
* --path_image2 is a path to the second image of the pair
* --write_dir designates the directory where to save the images with the keypoints drawn on them
* --path_glam_weights is the path to the weights of the trained GLAMpoint model.

optional arguments are
* --SIFT is a bool stating if the same pipeline should be applied to the image pair when using SIFT detector (and descriptor). Default is True
* --NMS which is the non max suppression window, default is 10
* --min_prob which is the minimum probability that a pixel must have to be considered as a keypoint on the score map (output of the Reti model), default is 0
* --green_channel: bool to determine if we are using the green channel instead of gray image to compute the score map (default is True).


## GLAMPOINTS LICENSE CONDITIONS

Copyright (2019), RetinAI Medical AG.

This software for the training and application of Greedily Learned Matching keypoints is being made available for individual research use only. For any commercial use contact RetinAI Medical AG.

For further details on obtaining a commercial license, contact RetinAI Medical AG Office (sales@retinai.com). 

RETINAI MEDICAL AG MAKES NO REPRESENTATIONS OR
WARRANTIES OF ANY KIND CONCERNING THIS SOFTWARE.

This license file must be retained with all copies of the software,
including any modified or derivative versions.
>>>>>>> 54a9b6b918191207bf7a166226d7d6284548333c


## Reference

Should you make use of this work, please cite the paper accordingly:

```bash
@article{Truong2019GLAMpointsGL,
  title={GLAMpoints: Greedily Learned Accurate Match Points},
  author={Prune Truong and Stefanos Apostolopoulos and Agata Mosinska and Samuel Stucky and Carlos Ciller and Sandro De Zanet},
  journal={2019 IEEE/CVF International Conference on Computer Vision (ICCV)},
  year={2019},
  pages={10731-10740}
}
```